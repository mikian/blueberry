# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'blueberry/version'

Gem::Specification.new do |spec|
  spec.name          = 'blueberry'
  spec.version       = Blueberry::VERSION
  spec.authors       = ['Mikko Kokkonen']
  spec.email         = ['mikko@mikian.com']

  spec.summary       = 'HTML Document generator for API Blueprints'
  spec.description   = 'Generates pretty HTML Documentation from API Blueprint files.'
  spec.homepage      = 'https://github.com/mikian/blueberry'
  spec.license       = 'MIT'

  spec.files         = `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  spec.bindir        = 'exe'
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']

  spec.add_development_dependency 'bundler', '~> 1.10'
  spec.add_development_dependency 'rake', '~> 10.0'
  spec.add_development_dependency 'rspec', '~> 3.3.0'
  spec.add_development_dependency 'pry', '~> 0.10'
end
